import unittest
import numpy as np
import monitor.radio.iso_receiver as iso


class TestReceiver(unittest.TestCase):

    def setUp(self):
        self.rx = iso.IsoReceiver(None)

    def test_iso(self):
        iso_mat = np.array([[0.9, 0.1],[0.1,0.9]])
        refl = np.random.uniform() + 1j*np.random.uniform()
        noise = np.random.normal(size=(1024)) + 1j*np.random.uniform((1024))
        t = np.linspace(0, 2*np.pi, num=1024)
        rx_1 = np.sin(t) + 1j*np.cos(t)
        rx_2 = refl*rx_1+noise

        # with iso
        X,Y = receiver.iso(iso_mat, rx_1, rx_2)
        self.assertTrue(len(X) > 1)
        self.assertTrue(len(Y) > 1)

        # TODO add more

    def test_get_spectrum(self):
        bins,labels = self.rx.get_spectrum()
        self.assertTrue(type(bins) is not None)
        self.assertTrue(type(labels) is not None)
        self.assertTrue(len(bins) > 1)
        self.assertTrue(len(labels) > 1)
        self.assertTrue(len(bin) == len(labels))