import receiver
import time
import numpy as np

TEST1 = False
TEST2 = False
TEST3 = True

# test 1 -------
# example output
# TEST1 - Total time to run 10 iterations: 49.750 s
# TEST1 - avg: 4.956, std: 0.109
if TEST1:
    tstart = time.time()
    iterations = 10
    times = np.empty((iterations))
    for ii in range(iterations):
        it_tstart = time.time()
        rx = receiver.Receiver()
        times[ii] = time.time() - it_tstart
        del rx

    tend = time.time()
    print("TEST1 - Total time to run {} iterations: {:.3f} s".format(iterations, tend-tstart))
    print("TEST1 - avg: {:.3f}, std: {:.3f}".format(times.mean(), times.std()))

# TEST2 - The device is just-reinitialized and does not run the full firmware-init
# (what does it actually do?)
# it saves only about 1.5 seconds to not garbage collect. The amount of output is misleading
# since much less is displayed in this run
#
# TEST2 - Total time to run 10 iterations: 36.717 s
# TEST2 - avg: 3.672, std: 1.614
if TEST2:
    tstart = time.time()
    iterations = 10
    times = np.empty((iterations))
    for ii in range(iterations):
        it_tstart = time.time()
        rx = receiver.Receiver()
        times[ii] = time.time() - it_tstart
        #del rx # NO GC

    del rx
    tend = time.time()
    print("TEST2 - Total time to run {} iterations: {:.3f} s".format(iterations, tend-tstart))
    print("TEST2 - avg: {:.3f}, std: {:.3f}".format(times.mean(), times.std()))

# TEST 3
# TEST3 - Total time to run 10 iterations: 74.455 s
# TEST3 - avg: 7.444, std: 0.052
# TEST3 - num steps total 1930, per iteration 193
# TEST3 - num bins total 1280, per iteration 128
if TEST3:
    rx = receiver.Receiver()
    tstart = time.time()
    iterations = 10
    times = np.empty((iterations))
    sum_steps = 0
    sum_bins = 0
    for ii in range(iterations):
        it_tstart = time.time()
        bins,labels = rx.get_spectrum()
        sum_steps += bins.shape[0]
        sum_bins += bins.shape[1]
        times[ii] = time.time() - it_tstart
        

    print(bins.shape)
    del rx
    tend = time.time()
    print("TEST3 - Total time to run {} iterations: {:.3f} s".format(iterations, tend-tstart))
    print("TEST3 - avg: {:.3f}, std: {:.3f}".format(times.mean(), times.std()))
    print("TEST3 - num steps total {}, per iteration {}".format(sum_steps,sum_steps/iterations ))
    print("TEST3 - num bins total {}, per iteration {}".format(sum_bins,sum_bins/iterations ))
