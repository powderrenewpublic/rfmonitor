import unittest
import device
import receiver
import numpy as np

class TestDevice(unittest.TestCase):

    def setUp(self):
        self.device  = device.Device()
    
    def test_get_temp(self):
        temp = self.device.get_temperature()
        self.assertTrue(temp > 0 and temp != 0)

    def test_get_RSSI(self):
        rssi = self.device.get_rssi()
        self.assertTrue(rssi < 0 and rssi != 0)

    def test_get_serial_number(self):
        sn = self.device.get_serial_number()
        self.assertTrue(sn > 0 and sn != 0)

    def test_get_LO_locked(self):
        locked = self.device.get_LO_locked()
        self.assertIs(type(locked), bool)

    def test_get_samples_single_chan_test(self):
        samps = self.device.get_samples_single_chan(2400e6, 0)
        self.assertTrue(len(samps) > 0)

    def test_get_samples_cont(self):
        samps = self.device.get_samples_cont(2400e6)
        self.assertTrue(len(samps) > 0)

    def test_get_samples_stepped(self):
        samps = self.device.get_samples_stepped(2400e6)
        self.assertTrue(len(samps) > 0)

class TestReceiver(unittest.TestCase):

    def setUp(self):
        self.rx = receiver.Receiver()

    def test_iso(self):
        iso_mat = np.array([[0.9, 0.1],[0.1,0.9]])
        refl = np.random.uniform() + 1j*np.random.uniform()
        noise = np.random.normal(size=(1024)) + 1j*np.random.uniform((1024))
        t = np.linspace(0, 2*np.pi, num=1024)
        rx_1 = np.sin(t) + 1j*np.cos(t)
        rx_2 = refl*rx_1+noise

        # with iso
        X,Y = receiver.iso(iso_mat, rx_1, rx_2)
        
        # continue

    def test_get_spectrum(self):
        bins,labels = self.rx.get_spectrum()
        self.assertTrue(type(bins) is not None)
        self.assertTrue(type(labels) is not None)
        self.assertTrue(len(bins) > 1)
        self.assertTrue(len(labels) > 1)
        self.assertTrue(len(bin) == len(labels))
        

if __name__ == '__main__':
    unittest.main()

    
