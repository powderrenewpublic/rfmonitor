import cProfile
import receiver
import device

# run get_spectrum
rx = receiver.Receiver()
print('profiling receiver get_spectrum')
cProfile.run('rx.get_spectrum()')
del rx

dev = device.Device()
print('profiling device get_spectrum')
cProfile.run('dev.get_samples_cont(int(2400e6))')
del dev

print('profiling receiver init')
cProfile.run('receiver.Receiver()')
