import unittest
import monitor.net.data_out_server as doserver
import monitor.net.data_in_server as diserver
import monitor.net.data_message as message
import numpy as np
import time
from multiprocessing import Process 


ip = "localhost"
port = 8128

# TODO
class TestDevice(unittest.TestCase):

    def setUp(self):
        self.doserve = doserver.DataOutServer(ip, port)
        self.diserve = diserver.DataInServer(ip, port)
        p = Process(target = self.diserve.listen)
        p.start()

    def test_send(self):
        status = self.doserve.serialize_and_send_data(np.array(["1","2"]),
                np.array(["1","2"]),
                np.array(["1.0","2.0"]),
                np.array(["1.0","2.0"]))
        self.assertTrue(status != "")