import tx_calibration_server as tx_cs
import rx_calibration_client as rx_cc
import numpy as np
import unittest

class TestCalServer(unittest.TestCase):

    def setUp(self):
        self.calserver = tx_cs.TX_Calibration_Server()

    def test_get_params(self):
        temp = self.calserver.get_params()
        self.assertTrue(len(temp) > 0)

    def test_get_wf(self):
        temp = self.calserver.get_waveform()
        self.assertTrue(len(temp)> 0)

    def test_get_actual_freq(self):
        f = self.calserver.get_actual_freq()
        self.assertTrue(f >= 0)

    def test_bcast1(self):
        t = self.calserver.broadcast_sine_chan1(2400e6, 1)
        self.assertTrue(type(t) is str)

    def test_bcast0(self):
        t = self.calserver.broadcast_sine_chan0(2400e6, 1)
        self.assertTrue(type(t) is str)


class TestCalClient(unittest.TestCase):


    def setUp(self):
        self.client = rx_cc.RX_Calibration_Client()
        
    def test_1_cal(self):
        errors = self.client.calibrate(2400e6, 1)
        self.assertTrue(errors == 0, "num errors: {}".format(errors))


if __name__ == "__main__":
    unittest.main()
