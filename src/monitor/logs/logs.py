import logging
import datetime
import os

STORAGE_DIR = "./log_files/"
LOG_FNAME = "monitor_log_" + str(datetime.date.today()) + ".log" 

class Logs():
    def __init__(self, log_directory=STORAGE_DIR, log_fname=LOG_FNAME):
        log_fname = os.path.join(log_directory, log_fname)
        if not os.path.exists(log_directory):
            os.makedirs(log_directory)
        logging.basicConfig(filename=log_fname, level=logging.DEBUG, format='%(asctime)s %(message)s')

    def log_successful(self, message):
        logging.info("[ ] successful" + str(message))

    def log_unsuccessful(self, message):
        logging.warning("[!] unsuccessful" + str(message))

    def log_other(self, message):
        logging.info("[ ] " + str(message))
