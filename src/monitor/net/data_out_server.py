import socket, sys
from . import net_constants as nc


class DataOutServer():
    """DataOutServer TODO rename DataOutInterface
    Represents the server that receives output from the monitor. 
    """
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        

    def serialize_and_send_data(self, devs, timestamps, freqs, powers, cfreqs):
        """Non-blocking method that provides serialized data over the network 

        Parameters:
            devs - device ids associated with each freq-power pair
            timestamps -  timestamps associated each freq-power pair
            freqs - the frequency at which power is measured
            powers - the power level in decibels at the associated frequency

        Returns: 
            status - a string representing the status of the method
        
        """
        message = self._create_message(devs, timestamps, freqs, powers, cfreqs)
        message_len = sys.getsizeof(message.encode())
        status_str = ""

        try: # set up comms with emulab system 
            sock = socket.create_connection((self.ip, self.port), nc.CONNECT_TIMEOUT)
            print("[DATA_OUT_SERVER] connecting to ({},{})".format(self.ip, self.port))
            sock.send(nc.INITIAL_MESSAGE.encode())
            print("[DATA_OUT_SERVER] sent init message")
            print("[DATA_OUT_SERVER sending {} bytes to server".format(message_len))
            totalsent = 0
            while totalsent < len(message):
                nbytes_sent = sock.send(message[totalsent:].encode())
                totalsent += nbytes_sent
            status_str = "Reported {} bytes to server\n".format(totalsent)
            print("[DATA_OUT_SERVER] " + status_str)
            sock.send(nc.TERM_MESSAGE.encode())
            print("[DATA_OUT_SERVER] Sent term message")
            sock.close()
        except socket.error as e:
                print("[MONITOR] Error: {}. Is {}:{} up?".format(e,self.ip, self.port))
                print("[MONITOR] Will attempt connection again")
                status_str = "Socket error\n"
        return status_str

    def _create_message(self, devs, timestamps, freqs, powers, cfreqs):
        """Create a comma-separated, newline-terminated string from the received spectrum
        
        Parameters:
            devs - device ids associated with each freq-power pair
            timestamps -  timestamps associated each freq-power pair
            freqs - the frequency at which power is measured
            powers - the power level in decibels at the associated frequency
            cfreqs - the center frequency at the associated frequency

        Returns: 
            message - a multi-line string with one (dev,ts,freq,power) tuple per row
        
        """
        message_str = ""
        #print("[DEBUG] {} {} {} {}".format(devs.shape, timestamps.shape, freqs.shape, powers.shape))

        for ii in range(freqs.shape[0]):
            # create one row
            strrow = "{},{},{},{},{}\n".format(devs[ii], timestamps[ii], freqs[ii], powers[ii], cfreqs[ii])
            message_str += strrow

        return message_str
