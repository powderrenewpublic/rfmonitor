import ntplib
import time

class TimeUtils():
    """Use this to get a common time

    TimeUtil uses NTP to get a time offset from an NTP server. When asking
    for time, it applies the offset to the current (system) time. 
    """
    def __init__(self):
        self._client = ntplib.NTPClient()
        response = self._client.request('us.pool.ntp.org', version=3)
        self.offset = response.offset

    def _sync(self):
        """ Re-sync with the NTP server
        """
        self.offset = self._client.request('us.pool.ntp.org', version=3).offset
        
    def get_time_now(self):
        """Return the synchronized time in number of seconds since epoch
        """
        return time.time() + self.offset

    def get_str_time_now(self):
        """Return the synchronized time in an easy to read form
        
        e.g. `Thu Mar 28 13:43:39 2019` 
        """
        return time.ctime(time.time() + self.offset)
