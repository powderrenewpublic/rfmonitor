import numpy as np
import scipy.signal 



def iso(iso_matrix, rx_1_signal, rx_2_signal):
    """Compute X and Y given RX1, RX2 and the isolation matrix
      iso matrix: [ a b ]
                  [ c d ]


      [R1 R2][ d -b]      1      = [X Y]
             [-c  a]   (ad-bc)


      X = c *  d*R1 + -c*R2
      Y = c * -b*R1 +  a*R2

    """
    det_sc = 1/np.linalg.det(iso_matrix)
    X = det_sc * (iso_matrix[1,1]*rx_1_signal - iso_matrix[1,0]*rx_2_signal)
    Y = det_sc * (-iso_matrix[0,1]*rx_1_signal + iso_matrix[0,0]*rx_2_signal)
    return X,Y

def dft(nfft, samples):
    """return the discrete fourier transform of `samples`
    NOTE: 
     DFT does not fftshift, or scale, or otherwise process the samples. 
     * Makes more sense for PSD signals which represent something more physical.
     * DFT is just a mathematical operation.
     * 
    """
    window = np.hamming(nfft)
    result = np.multiply(window, samples)  # windowing reduces aliasing due to finite length
    result = np.fft.fft(result, nfft) # compute DFT
    return result

def dft_mat(nfft, samples_mat):
    # TODO does np.fft and np.multiply support matrix ops so that this method isn't needed?
    outmat = np.empty(samples_mat.shape, dtype=np.complex64)
    for idx,row in enumerate(samples_mat):
        outmat[idx,:] = dft(nfft, row)
    return outmat


def psd(nfft, samples, center_freq, n_remove, Fs=30.72e6):
    """Return the power spectral density of `samples`"""
    dft_samples = dft(nfft, samples)
    psd, labels = dft_to_psd(nfft, dft_samples, center_freq, n_remove, Fs)
        
    return psd, labels

def psd_welch(nfft, samples, center_freq, n_remove, Fs=30.72e6):
    """ use welch's method to estimate PSD
    """
    labels0, psd0lin = scipy.signal.welch(samples[0,:], Fs, nperseg=nfft, return_onesided=False)
    labels1, psd1lin = scipy.signal.welch(samples[1,:], Fs, nperseg=nfft, return_onesided=False)
    
    psd0 = np.nan_to_num(10.0 * np.log10(psd0lin))
    psd1 = np.nan_to_num(10.0 * np.log10(psd1lin))
    labels0 = labels0 + center_freq
    labels1 = labels1 + center_freq

    # put DC in the center
    psd0 = np.fft.fftshift(psd0)
    psd1 = np.fft.fftshift(psd1)
    labels0 = np.fft.fftshift(labels0)
    labels1 = np.fft.fftshift(labels1)

    psd = np.vstack((psd0,psd1))
    labels = np.vstack((labels0, labels1))

    # reject samples above cutoff
    # * (high frequencies are near fn=f/Fs=-0.5 and 0.5
    #    or rather the beginning and end of an FFT-shifted array)
    psd = psd[:, n_remove:-n_remove] 
    labels = labels[:, n_remove:-n_remove]

    return psd, labels

def dft_to_psd(nfft, dft, center_freq, n_remove, Fs=30.72e6):
    """Return the power spectral density of the given DFT"""
    result = (1/(nfft*Fs))*np.square(np.abs(dft))+1e-20 # compute the power spectrum 
    result = np.nan_to_num(10.0 * np.log10(result)) # convert to decibels
    labels = np.fft.fftfreq(nfft, 1/Fs) # labels = np.arange(-Fs//2, Fs//2, Fs/nfft)
    
    # put DC in the center
    result = np.fft.fftshift(result)
    labels = np.fft.fftshift(labels)

    # adjust to actual frequency
    labels = labels + center_freq

    # reject samples above cutoff
    # * (high frequencies are near fn=f/Fs=-0.5 and 0.5
    #    or rather the beginning and end of an FFT-shifted array)
    result = result[n_remove:-n_remove] 
    labels = labels[n_remove:-n_remove]

    return result, labels

def dft_to_psd_2_chan(nfft, dft, center_freq, n_remove, Fs=30.72e6):
    psd0,labels0 = dft_to_psd(nfft, dft[0,:], center_freq, n_remove, Fs)
    psd1,labels1 = dft_to_psd(nfft, dft[1,:], center_freq, n_remove, Fs)
    psd = np.vstack((psd0,psd1))
    labels = np.vstack((labels0, labels1))
    return psd, labels

def mean_complex(X):
    """ X is a matrix.
    Mean will be computed over rows 
    
    For example, if X has shape: (10, 512) 
    X_avg = (X[0,:] + X[1,:] + ... + X[9,:])/10
    X_avg has shape: (1,512)
    """
    k = X.shape[0]
    M = X.shape[1]
    out = np.zeros((1,M), dtype=np.complex64)
    
    for row in X:
        out += row
    out /= k
    
    return out

    
def mov_avg_filt(signal, order):
    kern = np.ones((order))/order
    return np.convolve(signal, kern, 'same')
        
def energy(signal):
    """ Calculate the energy of a complex signal """
    return np.sum(np.square(np.abs(signal)))

def bin_avg(nbins, bins, labels):
    """average over len(`bins`)/`nbins` entries in `bins`.
       Sample the labels at the midpoint of the old bin.
       Typical useage of this is for averaging a PSD.
    """
    binw = len(bins)//nbins
    new_bins = np.empty(nbins)
    new_bins_sd = np.empty(nbins)
    new_labels = np.empty(nbins)
    for i in range(nbins):
        new_bins[i] = np.mean(bins[i*binw : (i*binw)+binw]) # avg of old bins
        new_bins_sd[i] = np.std(bins[i*binw : (i*binw)+binw])  # standard deviation
        new_labels[i]= np.mean(labels[i*binw : (i*binw)+binw]) # midpoint among old labels
    return new_bins, new_labels, new_bins_sd

def adjust_uncertainty(bins, uncert_per_bin, uncert_factor):
    """ Adjust the power based on the uncertainty in measurements 
    If UNCERT_FACTOR is positive, power measurements are biased to be higher, decreasing false negatives
    If UNCERT_FACTOR is negative, power measurements are biased to be lower, decreasing false positives
    """
    if uncert_per_bin.any() < 0:
        raise Exception("uncertainty measurements must be positive-valued")
    return bins + uncert_factor*uncert_per_bin 

def get_full_freq_list_centered(fft_size, sample_rate, center_freq):
    """ returns frequencies on full data for one collect with fftshift applied
    Ex.
    For fft size 8192, sample rate 30.72e6, computes an array of size 8192:
    [-15.36 MHz, -15.35625 MHz, -15.3525 MHz, ... 15.35625 MHz, 15.36 MHz]

    To get the actual frequencies, these are added to the center frequency:
    For 2.4 GHz f_c,
    [2384.64 MHz, 2384.64375 MHz, ..., 2415.34875 MHz, 2415.3525 MHz]

    This final vector is returned
    """
    single_bin_width = sample_rate/fft_size
    half_interval_width = fft_size//2
    adj_freq = np.array(range(-half_interval_width,half_interval_width))*single_bin_width
    return adj_freq + center_freq

def average_PSD_matrix(N_rows, N_output_bins_per_row, psd_matrix_high_res, freq_matrix_high_res):
        """Given a high resolution spectral measurement matrix,
        return a smoothed, downsampled version.
        """
        # Setup the output arrays 
        # rows: 1 row per step
        # columns: downsampled (averaged) PSD values
        avgd_psd_matrix = np.empty((N_rows, N_output_bins_per_row))
        labels_matrix = np.empty((N_rows,  N_output_bins_per_row))
        uncert_matrix = np.empty((N_rows,  N_output_bins_per_row))
        
        # compute averages
        for idx, psd_rows in enumerate(psd_matrix_high_res):
            
            new_bins, new_labels, new_bins_sd = bin_avg(N_output_bins_per_row, psd_rows, freq_matrix_high_res[idx,:])

            labels_matrix[idx, :] = new_labels
            avgd_psd_matrix[idx, :] = new_bins
            uncert_matrix[idx, :] = new_bins_sd

        return avgd_psd_matrix, labels_matrix, uncert_matrix

def cross_corr_lag(X,Y):
    """
    X: first signal
    Y: second signal
    
    returns: lag value where Xcorr is maximal

    to time align signals, take output and circshift (np.roll) Y by the lag 
    a negative lag means to shift to the left
    positive is a shift to the right
    """
    X_norm = (X-np.mean(X))/np.std(X)
    Y_norm = (Y-np.mean(Y))/np.std(Y)
    corr_seq = np.abs(np.correlate(X_norm,Y_norm,'full')) # use abs for complex sequences
    lag = corr_seq.argmax() - (len(X) - 1)
    return lag

def circshift(data, lag):
    """
    circularly shift a vector called 'data'
    by amount in 'lag'
    """
    
    if len(data.shape) > 1:
        raise Exception("circshift requires a vector. Use np.roll(data, shift, axis) to shift N-D data")
    return np.roll(data, lag)


if __name__ == "__main__":
    # quick test
    X = np.array([0,0,0,1,1,1,0,0,0,0])
    Y = np.array([0,0,0,0,0,0,1,1,1,0])
    lag = cross_corr_lag(X,Y)
    print(lag)
    print(X)
    print(Y)
    print(circshift(Y,lag))
