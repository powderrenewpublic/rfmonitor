import numpy as np
import uhd



class TX_Device():
    """
    Device represents a B210 USRP SDR device that can transmit samples. 

    Parameters:

    """
    
    def __init__(self, addr=""):

        # setup USRP radio device
        self.usrp = uhd.usrp.MultiUSRP(addr)  
        print("[TX DEVICE] USRP radio initialized successfully")        
        
   
    def transmit_waveform(self, wf_proto, duration, freq, rate=1e6, channel=(0,), gain=60):
        """ Use this to transmit a waveform
        
        Parameters
        * waveform_proto: numpy array of samples to TX
        * duration: time in seconds to transmit at the supplied rate
        * freq: TX frequency (Hz)
        * rate: TX sample rate (Hz)
        * channels: list of channels to TX on
        * gain: TX gain (dB)
        
        returns: the number of transmitted samples
        """
        n_transmitted_samps = self.usrp.send_waveform(wf_proto, duration, freq, rate, channel, gain)
        return n_transmitted_samps