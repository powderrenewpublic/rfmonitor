#!/usr/bin/env python3

from ..radio import iso_receiver, constants as C, signal_utils as su
import matplotlib.pyplot as plt
import numpy as np
import time

#plt.axis([0,8500,-120,0])
plt.ion()
plt.title('IsoViewer')
plt.show()

class IsoRXViewer():
    def __init__(self, cal_file, rx_gain=60, threshold_dB=-110):
        self.iso_rx = iso_receiver.IsoReceiver({"nuc2":"a"}, cal_file, False)
        self.threshold_dB = threshold_dB
        actual_gain = self.iso_rx.set_gain(rx_gain)
        print("\033[93m[VIEWER] requested gain {}. actual gain {} \033[0m".format(rx_gain, actual_gain))

    def start_streaming(self):
        line = None
        while True:
            start = time.time()
            devs, times, labels, powers = self.iso_rx.get_full_spectrum()
            end = time.time()

            nfreqs = np.array(labels)
            npowers = np.array(powers)
            args2400 = np.nonzero((nfreqs > 2399.9) & (nfreqs < 2400.1))
            args_above_thresh = np.nonzero(npowers > self.threshold_dB)
            
            print("")
            print("\033[93m[VIEWER] frequencies above threshold {}: [{}]  \033[0m ".format(self.threshold_dB, nfreqs[args_above_thresh]))
            print("\033[93m[VIEWER] power @cf = {}, {} \033[0m".format(nfreqs[args2400], npowers[args2400]))
            print("\033[93m[VIEWER] Elapsed time: {} \033[0m".format(end-start))

            if line:
                l = line.pop(0)
                l.remove()
       
            line = plt.plot(labels, powers, 'b')      
            plt.legend(('psd '))

            plt.draw()
            plt.pause(0.01)

    

