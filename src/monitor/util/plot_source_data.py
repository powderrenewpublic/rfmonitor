#!/usr/bin/python2
#
# Author: BCT
# Date last modified: 8/13/2018
#
# Use this to open and view a complex-valued binary (I/Q interleaved) data file

import sys
import matplotlib.pyplot as plt
import numpy.matlib as mb
import numpy as np

# setup the transmitter
WAVE_AMPL = 0.7
GAIN0 = 75
GAIN1 = 55
DURATION = 5  # sec
RATE = 1e6
FREQ = 2400e6
SRATE = 30.72e6

INTERP=SRATE/RATE

WAVE_FREQ = 10e3/INTERP

WAVEFORMS = {
    "sine": lambda n, tone_offset, rate: np.exp(n * 2j * np.pi * tone_offset / rate),
    "square": lambda n, tone_offset, rate: np.sign(waveforms["sine"](n, tone_offset, rate)),
    "const": lambda n, tone_offset, rate: 1 + 1j,
    "ramp": lambda n, tone_offset, rate:
            2*(n*(tone_offset/rate) - np.floor(float(0.5 + n*(tone_offset/rate))))
}

SINE_DATA = np.array(
    list(map(lambda n: WAVE_AMPL * WAVEFORMS["sine"](n, WAVE_FREQ, RATE),
             np.arange(
                 int(10 * np.floor(RATE / WAVE_FREQ)),
                 dtype=np.complex64))),
    dtype=np.complex64)  # One period


# make 10 periods of the waveform
signal = mb.repmat(SINE_DATA, 1, 10)
signal = np.squeeze(signal)
signal = signal[0:8196]

rx1r = np.real(signal)
rx1i = np.imag(signal)

fig, axs = plt.subplots(1,1, sharex=True, sharey=True)
x = range(len(rx1r))
axs.plot(x, rx1r, x, rx1i)
axs.set_title('chan0 - RX1')

plt.show()
