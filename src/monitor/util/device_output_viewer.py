#!/usr/bin/env python3

from ..radio import device, constants as C, signal_utils as su
import matplotlib.pyplot as plt
import numpy as np
import time

#plt.axis([0,8500,-120,0])
plt.ion()
plt.show()
lines = None


class DeviceViewer():
    """ 
    Params:
     * avg - how many times to average the signal
    """
    def __init__(self, gain=C.GAIN, num_samples=C.FFT_SIZE, rate=C.MAX_INST_BW, channels=C.CHANNELS, ants=("TX/RX", "TX/RX"), avg=1):
        self.gain=gain
        self.num_samples = num_samples
        self.rate = rate
        self.channels = channels
        self.avg = avg
        self.dev = device.Device(gain=gain, rate=rate, num_samps_req=num_samples, channels=channels, RXA=ants[0], RXB=ants[1])

    def start_streaming(self, frequency, method='psd', time_or_freq="freq", sweep=False):
        line1 = None
        line2 = None
        if time_or_freq == "time":
            while True:
                N=self.num_samples
                samps = self.dev.get_N_samples(frequency, N=self.num_samples)
                samps = np.real(samps)

                if line1:
                    line1.remove()
                if line2:
                    line2.remove()
            
                #import pdb; pdb.set_trace()
                labels = range(len(samps[0,:]))
                #line1,line2 = plt.plot(range(len(psd[0])), psd[0,:], 'b',range(len(psd[0])), psd[1,:]-4, 'r')
                line1,line2 = plt.plot(labels, samps[0,:], 'b',labels, samps[1,:]-0.01, 'r')
                plt.legend(('chan 1', 'chan 2'))
                plt.draw()
                plt.pause(0.01)
        else:
            niter=0
            while True:
                N=self.num_samples*self.avg
                niter += 1
                if niter % 10 == 0:
                    mean = np.mean(np.array(self.dev.times))
                    std = np.std(np.array(self.dev.times))
                    print(f"\ntime stats: mean {mean}  std {std}")
                    mean = np.mean(np.array(self.dev.drops))
                    std = np.std(np.array(self.dev.drops))
                    print(f"drops stats: mean {mean}  std {std}")
                if method=='psd':
                    if sweep:
                        psd1,labels1 = self.dev.get_spectrum(frequency+self.rate, N=self.num_samples, est_method='bartlett', avg=self.avg)
                        psd2,labels2 = self.dev.get_spectrum(frequency, N=self.num_samples, est_method='bartlett', avg=self.avg)
                        psd3,labels3 = self.dev.get_spectrum(frequency-self.rate, N=self.num_samples, est_method='bartlett', avg=self.avg)
                        psd = np.hstack((psd1, psd2, psd3))
                        labels = np.hstack((labels1,labels2, labels3))

                    else:
                        psd,labels = self.dev.get_spectrum(frequency, N=self.num_samples, est_method='bartlett', avg=self.avg)

                else:
                    if sweep:
                        psd1,labels1 = self.dev.get_spectrum(frequency+self.rate, N=self.num_samples, est_method='welch', avg=self.avg)
                        psd2,labels2 = self.dev.get_spectrum(frequency, N=self.num_samples, est_method='welch', avg=self.avg)
                        psd3,labels3 = self.dev.get_spectrum(frequency-self.rate, N=self.num_samples, est_method='welch', avg=self.avg)
                        psd = np.hstack((psd1, psd2, psd3))
                        labels = np.hstack((labels1,labels2, labels3))

                    else:
                        psd,labels = self.dev.get_spectrum(frequency, N=self.num_samples, est_method='welch', avg=self.avg)
            
                if line1:
                    line1.remove()
                if line2:
                    line2.remove()
            
                #line1,line2 = plt.plot(range(len(psd[0])), psd[0,:], 'b',range(len(psd[0])), psd[1,:]-4, 'r')
                line1,line2 = plt.plot(labels[0,:], psd[0,:], 'b',labels[1,:], psd[1,:]-0.5, 'r')
                plt.legend(('chan 1', 'chan 2'))
                plt.draw()
                plt.pause(0.01)


    def start_sweeping(self, freq_list):
        line1 = None
        line2 = None
        #import pdb
        #pdb.set_trace()
        while True:
            
            all_samples = np.empty((len(freq_list), 2, C.N), dtype=np.float32) # DFT -> needs fft_size instead of N (3,2,460)
            all_labels = np.empty((len(freq_list), 2, C.N), dtype=np.float32)
            tstart = time.time()
            for ii, center_freq in enumerate(freq_list):
                psd_samples, labels_Hz = self.dev.get_spectrum(int(center_freq)) # (2,460), (2,460)

                # cnt = 0
                # while cnt < 10:
                #     psd_samples, labels_Hz = self.dev.get_spectrum(int(center_freq)) # (2,460), (2,460)
                #     time.sleep(1)
                #     cnt += 1

                #print(self.dev.get_center_freq()/1e6)
                all_samples[ii,:, :] = psd_samples
                all_labels[ii,:, :] = labels_Hz            
            tend = time.time()
            print("duration {}".format(tend-tstart))
            psd1 = np.ravel(all_samples[:,0,:]) # (2760) should be (2,460*3=1380) 
            psd2 = np.ravel(all_samples[:,1,:])
            psd = np.vstack((psd1, psd2))
            
            labels_MHz = all_labels / 1e6
            labels1 = np.ravel(labels_MHz[:,0,:])
            labels2 = np.ravel(labels_MHz[:,1,:])
            labels = np.vstack((labels1, labels2))
            
            if len(psd.shape) == 1 or psd.shape[0] == 1:
                if line1:
                    l = line1.pop(0)
                    l.remove()
                line1 = plt.plot(labels[0], psd[0], 'b')      
                plt.legend(('chan 1'))
            else:
                if line1:
                    line1.remove()
                if line2:
                    line2.remove()
                line1,line2 = plt.plot(labels[0,:], psd[0,:], 'b',labels[1,:], psd[1,:]-4, 'r')
                plt.legend(('chan 1', 'chan 2'))
            plt.draw()
            plt.pause(0.01)
            


    
if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        raise Exception("provide a center frequency")
    center_freq = float(sys.argv[1])
    devview = DeviceViewer()
    devview.start_streaming(center_freq)
