import json
import time
import sys
import signal
import argparse
import os
import os.path

import monitor
import monitor.logs as logs
import monitor.storage as storage
import monitor.net.data_out_server as dos
import monitor.radio.range as R

COLLECT_PERIOD = 5 # second(s)
CONFIGDIR_SUBSTRING = "etc/rfmonitor"
DEVICE_CONFIG_FILENAME = "device_config.json"
CALDATA_FILENAME = "cal_data_ref.pkl"

# This is a hack, but it will help a bit.  Basically,
# ~/.local/etc/monitor/@basename takes precedence over either /usr/local
# or /etc .  If we can't find any config files, fall back to
# /etc/monitor/@basename .
def get_default_config_path(basename):
    paths = []
    if monitor.__file__.startswith("/usr/local"):
        paths.append("/usr/local/%s" % (CONFIGDIR_SUBSTRING))
    elif monitor.__file__.startswith("/usr/lib"):
        paths.append("/%s" % (CONFIGDIR_SUBSTRING,))
    homedir = os.getenv("HOME")
    if homedir:
        paths.insert(0,"%s/.local/%s" % (homedir,CONFIGDIR_SUBSTRING))
    for path in paths:
        fullpath = "%s/%s" % (path,basename)
        if os.path.exists(fullpath):
            return fullpath
    return "/%s/%s" % (CONFIGDIR_SUBSTRING,basename)

class MonitorApp():
    """ 
    Spectrum monitor application

    Periodically collects transmitter spectral data from the monitor SDR. 
    After sampling data, the transmitted signal is isolated from reflected and
    incident interference.  


    """
    def __init__(self, args, domock=False):
        self.data_out_net_port = args.port
        self.data_out_net_IP = args.server
        self.data_out_server = dos.DataOutServer(args.server, args.port)
        cal_file = args.calfile
        receiver_configf = args.configfile
        self.debug = args.debug
        self.nonetwork = args.nonetwork
        self.summarize = args.summarize
        self.onceonly = args.onceonly
        self.logdir = args.logdir
        self.output = args.output
        with open(receiver_configf, 'r') as jf:
            self.conf = json.load(jf)

        if self.output:
            self.outputfp = open(self.output, "w")

        # If there is a range argument, update the range constants.
        if args.range != None:
            (a,b) = args.range.split("-");
            R.START_FREQ = float(a)
            R.END_FREQ = float(b)
            pass

        if domock:
            import monitor.radio.mock_receiver as mock
            self.receiver = mock.MockReceiver(self.conf)
        else:
            import monitor.radio.iso_receiver as iso
            self.receiver = iso.IsoReceiver(self.conf, cal_file, args)
            print("[MONITOR] starting isoreceiver with conf: {}".format(self.conf))

    def watchdog_handler(self, signum, frame):
        print("Watchdog: receiver locked up, exiting");
        sys.exit(1)

    def start_monitoring(self):
        """This receiver will start monitoring the spectrum and will return spectrum measurements periodically.
        
        reports:
        A list of tuples consisting of device_id, timestamp, frequency, and power
        """
        signal.signal(signal.SIGALRM, self.watchdog_handler)

        while True:
            try:
                print("[MONITOR] periodic sample collect starting")
                signal.alarm(120)
                devs, timestamps, freqs, powers, cfreqs = self.receiver.get_full_spectrum()
                signal.alarm(0)
                if powers.mean() == 0 or freqs.mean() == 0:
                    raise Exception("[MONITOR] Error. All zeros returned from monitor: powers {}, freqs {}".format(powers.mean(),freqs.mean()))
                if self.summarize:
                    print("%s: %r %r/%r/%r %r/%r/%r"
                          % (",".join([str(x) for x in set(devs)]),
                             timestamps.mean(),
                             freqs.min(),freqs.mean(),freqs.max(),
                             powers.min(),powers.mean(),powers.max()))
                if self.nonetwork and not self.summarize:
                    for ii in range(freqs.shape[0]):
                        strrow = "{},{},{},{},{}".format(devs[ii],
                                                         timestamps[ii],
                                                         freqs[ii], powers[ii],
                                                         cfreqs[ii])
                        if self.output:
                            print(strrow, file=self.outputfp)
                            self.outputfp.flush()
                        else:
                            print(strrow)
                            pass
                        pass
                elif not self.nonetwork:
                    report_status = self.data_out_server.serialize_and_send_data(devs, timestamps, freqs, powers, cfreqs)
                    print("[MONITOR] status: {}".format(report_status))
                    pass
            except Exception as e:
                print("[MONITOR] status: {}".format(e))    
                raise(e)
            if self.onceonly:
                return
            time.sleep(COLLECT_PERIOD)

def parse_args():
    """Parse the command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--server", default="127.0.0.1", type=str)
    parser.add_argument("-p", "--port", default=9000, type=int)
    parser.add_argument("-l", "--logdir", default="/var/log/", type=str)
    parser.add_argument("-c", "--configfile", default=get_default_config_path(DEVICE_CONFIG_FILENAME), type=str)
    parser.add_argument("-i", "--calfile", default=get_default_config_path(CALDATA_FILENAME), type=str)
    parser.add_argument("-d", "--debug", default=False, action='store_true')
    parser.add_argument("-n", "--nonetwork", default=False, action='store_true')
    parser.add_argument("-S", "--summarize", default=False, action='store_true')
    parser.add_argument("-o", "--onceonly", default=False, action='store_true')
    parser.add_argument("-g", "--gain", default=30, type=int)
    parser.add_argument("-f", "--output", default=None, type=str)
    parser.add_argument("-R", "--range", default=None, type=str)
    return parser.parse_args()

"""
Entry point to the application
"""
def main():
    args = parse_args()
    monApp = MonitorApp(args, domock=False)
    monApp.start_monitoring()
