from scipy.signal import max_len_seq 
from numpy.fft import fft, ifft
import numpy as np

def pn_code_gen(period=8192):
    """ Assume that period i
    """
    bits = int(np.log2(period)) # MLS return length 2**n-1, still return the right amount of bits though
    seq = max_len_seq(bits, state=None) # initial state will be set to all ones if None given

    # TODO handle period edge cases (more zeros added in high f)

    # we aren't interested in the final state, just return the sequence
    seq = seq[0]

    # put zero at high freq to make periodic with 2^N instead of 2^N-1
    seqF = fft(seq)
    seqF = np.append(seqF, 0) 
    seq = ifft(seqF)

    # for transmitting we don't want binary representation but +1/-1
    seq = 2*seq-1
    return seq
