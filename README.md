# Spectrum Monitor

This application uses UHD drivers to control an Ettus B210 software-defined-radio to periodically sample the entire available spectrum. The application takes RF samples from the radio, isolates the transmitted and incident components, computes the PSD of the transmitted component, and returns the power per frequency band to another daemon.

_Running the application_

After installing, simply run `python monitor_main.py`
Command line arguments include:
* `-s/--server`, which is the hostname of the server where results will be sent
* `-p/--port`, likewise for the port
* `-l/--logdir`, which is the directory where log files are stored
* `-c/--configfile`, which specifies the location of the antenna/device mapping config file
* `-i/--calfile`, which specifies the location of the calibration file. A default file be used if none is provided.


_Example config file_

```
{
    "nuc1":["A"],
    "nuc2":["A","B"]
}
```

Here, the key is the device name and the value is list of the RF front-ends used for transmitting. For a USRP B210, for instance, "A" refers to the first transmit front-end (channel 0), "B" refers to the second front-end (channel 1). With this configuration, the monitor will monitor nuc1's SDR on front-end A, nuc2's SDR on front-end A and nuc2's SDR on front-end B.


To run the monitor and a plotter to show the spectrum in real-time use the scripts/run_monitor.sh script.


