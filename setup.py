#!/usr/bin/env python

from setuptools import setup, find_packages
from codecs import open
import os.path
import sys

setup(
    name="rfmonitor",
    version="0.3.1",
    author="Boston Terry",
    author_email="boston.terry@utah.edu",
    url="https://gitlab.flux.utah.edu/powderrenewpublic/rfmonitor",
    description="An SDR-based RF spectrum monitor",
    long_description="This application uses UHD drivers to control an Ettus B210 software-defined-radio to periodically sample the entire available spectrum. The application takes RF samples from the radio, isolates the transmitted and incident components, computes the PSD of the transmitted component, and returns the power per frequency band to another daemon.",
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "Operating System :: POSIX :: Linux",
        "Topic :: Internet",
        "Topic :: System :: Networking",
        "Topic :: System :: Networking :: Monitoring",
        "Topic :: System :: Systems Administration",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7"
    ],
    keywords="link monitoring monitor network monitor",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    install_requires=["scipy","numpy","ntplib","matplotlib"],
    #extras_requires={"uhd":"uhd"},
    entry_points={
        "console_scripts": [
            "rfmonitor=monitor.app:main",
            "rfmonitor-calibration=monitor.calibration:main"
        ],
    }
)
