#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "[PACKAGE INSTALLER] This script must be run as root" 
   exit 1
fi

# add powder package repository before update
curl -sL https://repos.emulab.net/emulab.key | apt-key add -
. /etc/os-release
echo "deb https://repos.emulab.net/powder/${ID} ${UBUNTU_CODENAME} main" \
    > /etc/apt/sources.list.d/powder.list

# Make sure apt doesn't prompt us for anything.
export DEBIAN_FRONTEND=noninteractive
apt-get update -y

# install emulab hosted packages
apt-get install -y uhd-host python3-uhd

# install monitor deps as packages
apt-get install -y python3-scipy python3-numpy python3-matplotlib python3-ntplib

# set up SDR
if [ ! $(uhd_images_downloader) ] || [ ! $(uhd_usrp_probe) ]; then
    echo "[PACKAGE INSTALLER] Error setting up USRP device"
    exit 1
else
    echo "[PACKAGE INSTALLER] USRP Device setup"
fi

echo "[PACKAGE INSTALLER] finished"
exit 0
