#!/usr/bin/env python3

import monitor.radio.tx_device as device
import monitor.calibration.pncode as mls
import numpy as np
import numpy.matlib
import argparse, sys, os


Nrep=100
def extend_wf(insig, n):
    return np.squeeze(np.matlib.repmat(insig, 1,int(n)))

def parse_args():
    """Parse the command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--freq", default=1500e6, type=float)
    parser.add_argument("-l", "--seq_len", default=512, type=int)
    parser.add_argument("-r", "--rate", default=30.72e6, type=float)
    parser.add_argument("-d", "--duration", default=20, type=float)
    parser.add_argument("-g", "--gain", default=60, type=float)
    parser.add_argument("-c", "--channel", default=0, type=int)

    return parser.parse_args()

"""
Entry point to the application
"""        
if __name__ == "__main__":
    args = parse_args()
    print("[TXMLS] Duration: {}. N repetitions: {}. Total len: {}".format(args.duration, args.duration*args.rate//args.seq_len,  args.duration*args.rate))
    seq = mls.pn_code_gen(args.seq_len)
    seq_ex = extend_wf(seq, Nrep)
    

    try:
        dev = device.TX_Device()
        while True:
            dev.transmit_waveform(seq_ex, duration=args.duration, freq=args.freq, rate=args.rate, gain=args.gain, channel=(args.channel,))
    except KeyboardInterrupt:
        print("\nInterrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

