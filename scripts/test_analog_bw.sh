#!/usr/bin/env python3

import sys
import monitor.util.device_output_viewer as device_viewer

if len(sys.argv) < 2:
    raise Exception("provide a center frequency")
center_freq = float(sys.argv[1])

