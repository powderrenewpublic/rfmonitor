#!/usr/bin/env python3


import numpy as np
import uhd


CHANNELS = (0,1)
RATE = (30.72e6)
GAIN = 60
FREQ = 1500e6

STREAM_ARGS = uhd.libpyuhd.usrp.stream_args("fc32", "sc16")
STREAM_ARGS.channels = CHANNELS

INIT_DELAY = 0.05 # delay before rx to allow for time sync
TIMEOUT = 0.01 
PACKET_TIME = int(2040.0/RATE)
DYN = 60
REF = 0


# Notes on stream commands:
# > stream_now = True for multiple channels on a single streamer will fail to time align!
# > stream_now must always be False. 
class SimpleDevice():
    def __init__(self, gain=GAIN, rate=RATE, channels=CHANNELS, recv_frame_size=8176, num_recv_frames=50):
        self.gain = gain
        self.rate = rate
        self.chans = channels

        self.usrp = uhd.usrp.MultiUSRP("recv_frame_size={},num_recv_frames={}".format(recv_frame_size,num_recv_frames)) 
        
        # set to a default value
        for chan in self.chans:
            self.usrp.set_rx_rate(rate, chan)
            self.usrp.set_rx_freq(uhd.libpyuhd.types.tune_request(FREQ), chan)
            self.usrp.set_rx_gain(gain, chan)
        
        # set up buffers and data streamer
        self.streamer = self.usrp.get_rx_stream(STREAM_ARGS)
        self.dev_buffer_size = self.streamer.get_max_num_samps() 
        
        # synchronize channels
        # TODO update this after getting GPSDO installed. Sync to GPS clock if possible
        self.usrp.set_time_unknown_pps(uhd.types.TimeSpec(0.0))
        #gps_time = self.get_gps_time()
        #self.usrp.set_time_next_pps(uhd.uhd.types.TimeSpec(gps_time+1))

        # create start stream command
        stream_cmd = self.get_stream_start_cmd()
        self.streamer.issue_stream_cmd(stream_cmd)       
        
        return



    def get_stream_start_cmd(self):
        """ Create a start stream cmd based on the current time
        """
        self.usrp.set_time_unknown_pps(uhd.types.TimeSpec(0.0))
        stream_cmd = uhd.types.StreamCMD(uhd.types.StreamMode.start_cont)
        stream_cmd.stream_now = False
        stream_cmd.time_spec = uhd.types.TimeSpec(self.usrp.get_time_now().get_real_secs()+INIT_DELAY)
        return stream_cmd

    def stop_stream(self):
        """ Sends a stop command to the FPGA. Subsequent calls to recv will return 0 bytes.
        """
        stream_stop_cmd = uhd.types.StreamCMD(uhd.types.StreamMode.stop_cont)
        self.streamer.issue_stream_cmd(stream_stop_cmd)

    def restart_stream(self):
        """ Stops, then restarts streaming on the FPGA
        """
        self.stop_stream()
        self.recreate_stream()
        start_cmd = self.get_stream_start_cmd()
        self.streamer.issue_stream_cmd(start_cmd)

    def recreate_stream(self):
        self.streamer = self.usrp.get_rx_stream(STREAM_ARGS)

    def get_N_samples(self, frequency, N):
        """ Default method
        """

        N = int(N)
        ZERO_RX_SAMP_THRESHOLD = 50
        for chan in self.chans:
            self.usrp.set_rx_freq(uhd.libpyuhd.types.tune_request(frequency), chan)
        
        # data in the buffer is invalid, so empty
        recvd = 0
        garbage_buffer = np.empty((len(self.chans), self.dev_buffer_size))
        rx_metadata = uhd.types.RXMetadata()
        # dump 20 packets (usually 2040*20=40800 samples or about a millisecond worth of data (1.3ms))
        while recvd < (20*self.dev_buffer_size):
            num_recvd = self.streamer.recv(garbage_buffer, rx_metadata)
            recvd += num_recvd
        
        num_accum_samps = 0
        result = np.empty((len(self.chans), N), dtype=np.complex64)
        recv_buffer = np.empty((len(self.chans), self.dev_buffer_size), dtype=np.complex64)
        rx_metadata = uhd.types.RXMetadata()

        received_zero_samples_x_times_in_a_row = 0
        prev_num_rx_samps = 0

        while num_accum_samps < N:
            # copy from internal buffer to local recv buffer
            num_rx_samps = self.streamer.recv(recv_buffer, rx_metadata)

            # catch hang condition where no samples are received
            if num_rx_samps == 0 and prev_num_rx_samps == 0: 
                received_zero_samples_x_times_in_a_row += 1
            elif num_rx_samps == 0 and prev_num_rx_samps != 0:
                received_zero_samples_x_times_in_a_row = 1
            if received_zero_samples_x_times_in_a_row >= ZERO_RX_SAMP_THRESHOLD:
                print("[DEVICE] received zero samples {} times in a row. Restarting the stream".format(received_zero_samples_x_times_in_a_row))
                received_zero_samples_x_times_in_a_row = 0
                self.restart_stream()

            # if we received > 0 samples, store them to the result array        
            if num_rx_samps:
                real_samps = min(N - num_accum_samps, num_rx_samps)
                result[:, num_accum_samps:num_accum_samps + real_samps] = recv_buffer[:, 0:real_samps]
            num_accum_samps += num_rx_samps

            prev_num_rx_samps = num_rx_samps

        return result


    def get_N_samples_failure_mode1(self, frequency, N):
        """ Restart stream before each collect
        This method usually hangs after a few iterations. 
        """
        self.restart_stream()
        return self.get_N_samples(frequency, N)
        

import time, math
def stress_test(device_method, num_itrs, dsize, alternate_frequencies=False):
    tstart = time.time()
    cnt = 0
    n_samps = 0
    prctl = math.floor(num_itrs/100)
    while cnt < num_itrs:
        if cnt % prctl == 0:
            print("{:.2f} % completed".format(100*float(cnt)/num_itrs))
        samps = device_method(1500e6, dsize)
        cnt += 1
        n_samps += samps.shape[1]
    tend = time.time()
    telp = tend-tstart
    print("\nelapsed: " + str(telp))
    print("N samples: " + str(n_samps))
    print("Expected: " + str(num_itrs*dsize))
    print("Elapsed time per collect: " + str(telp/float(num_itrs)))


if __name__ == "__main__":

    sd = SimpleDevice()

    num_iterations = 10000
    num_samples_per_collect = 2**20
    stress_test(sd.get_N_samples, num_iterations, num_samples_per_collect)