#!/usr/bin/env python3

import sys
import monitor.radio.iso_receiver as rx

if len(sys.argv) < 2:
    raise Exception("provide a center frequency")
center_freq = float(sys.argv[1])

isorx = rx.IsoReceiver(None)
isorx.__get_spectrum_process_test__(center_freq)
