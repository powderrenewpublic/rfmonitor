#!/usr/bin/env python3

import monitor.util.iso_rx_output_viewer as iso_rx_viewer
import monitor.radio.constants as C
import argparse, sys, os

def parse_args():
    """Parse the command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--calfile", default="/usr/local/etc/monitor/cal_data_ref.pkl", type=str)
    parser.add_argument("-g", "--rxgain", default=C.GAIN, type=float)

    return parser.parse_args()


"""
Entry point to the application
"""        
if __name__ == "__main__":
    args = parse_args()
    try:
        rxview = iso_rx_viewer.IsoRXViewer(args.calfile, rx_gain=args.rxgain)
        #import pdb; pdb.set_trace()
        rxview.start_streaming()


    except KeyboardInterrupt:
        print("\nInterrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
