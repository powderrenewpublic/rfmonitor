#!/usr/bin/python3
#
# Author: BCT
# Date last modified: 8/13/2018
#
# Use this to open and view atime-series data file

import sys
import matplotlib.pyplot as plt
import numpy as np
import scipy
import pickle
import argparse

if len(sys.argv) < 2:
    print("Argument Error--")
    raise Exception ("Useage: plot_sample <file> [num_samples] [skip] [sample rate in Hz]")

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", required=True, type=str)
parser.add_argument("-n", "--num_samples", default=0, type=int, help="0 plots all samples")
parser.add_argument("-s", "--skip", default=0, type=int, help="skip this many samples from the start")
args = parser.parse_args()
skip = args.skip
num_samples = args.num_samples


with open (args.file, 'rb') as f:
    vec = np.array(pickle.load(f))


num_samps = num_samples if num_samples > 0 else vec.shape[0]

if vec.dtype==np.complex64:
    newvec = np.zeros(vec.shape[0], 2)
    newvec[:, 0] = np.real(vec)
    newvec[:, 1] = np.imag(vec)
    vec = newvec

#f = scipy.fromfile(data_file, dtype=scipy.complex64)

# check that we don't exceed the file length with requested number of samples
x = range(num_samps)
if skip + num_samps > vec.shape[0]:
    raise("skip + num_samples requested exceeds number of samples in file")

plt.plot(x,vec[skip:num_samps])
plt.show()
# plot the time-domain signal
#plt.subplot(211)
#plt.plot(x, realf[skip:skip+num_samples_to_plot])
#plt.plot(x, imagf[skip:skip+num_samples_to_plot])

# plot the frequency domain signal (shift so that 0 (DC) is in the middle of the axis)
#win = np.hamming(num_samples_to_plot)
#result = np.multiply(win, f[skip:skip+num_samples_to_plot])
#F = np.fft.fftshift(np.fft.fft(result))
#F = np.nan_to_num(10*np.log10(np.square(np.abs(F))))
#half_intv_len = num_samples_to_plot / 2
#X = range(-half_intv_len,half_intv_len)
#if use_sample_rate:
#    X = np.dot(np.dot(np.array(X),sample_rate),1.0/num_samples_to_plot)

#plt.subplot(212)
#plt.plot(X, F)
#plt.show()
