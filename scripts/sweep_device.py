#!/usr/bin/env python3

import sys
import monitor.util.device_output_viewer as device_viewer
import monitor.radio.constants as C
import numpy as np

# center_freqs = [1500e6, 1530e6, 1560e6]
center_freqs =  np.arange(700e6, 6000e6, C.ANALOG_BW)
devview = device_viewer.DeviceViewer()
devview.start_sweeping(center_freqs)
