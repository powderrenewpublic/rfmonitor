#!/usr/bin/env python3
import sys
import time
import argparse
import socket
import matplotlib
import matplotlib.pyplot as plt
import pickle
matplotlib.use('tkagg')
from io import StringIO
import csv
import numpy as np

"""
Useage: 
run monitor_main.py to start collecting and reporting data. 

(install matplotlib and python3-tk
- python3 -m pip install matplotlib
- sudo apt-get install python3-tk)

run this script to view the output of the monitor

"""
TERM_MSG="0,0,0,0"
def mon_recv(sock):
    chunks = ""
    iter = 0
    nbytes = 0
    while True:
        chunk =  sock.recv(2048)
        nbytes += sys.getsizeof(chunk)
        if TERM_MSG in chunk.decode():
            print("got term message")
            term_ind = chunk.decode().find(TERM_MSG)
            print("termin index: {}".format(term_ind))
            print("chunk: ".format(chunk.decode()[ max(term_ind-20, 0):min(len(chunk), term_ind+20) ] ))
            chunks += chunk.decode()[:max(term_ind-1, 0)]
            #print("adding to chunk: {}".format())
            return chunks
        chunks += chunk.decode()
        if iter % 100 == 0:
            print("[RECV] iterations: {}, last chunk: {}, total bytes: {}".format(iter, chunk[-30:], nbytes))
        iter += 1
    return chunks

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--server", default="127.0.0.1", type=str)
parser.add_argument("-p", "--port", default=9000, type=int)
parser.add_argument("-y", "--max_y", default=-80, type=int)
parser.add_argument("-m", "--min_y", default=-140, type=int)
parser.add_argument("-x", "--max_x", default=6000, type=int)
args = parser.parse_args()
port = args.port
server = args.server

plt.axis([0,args.max_x,args.min_y,args.max_y])
plt.title('Monitor Viewer')
plt.ion()
plt.show()
lines = None

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((server, port))
serversocket.listen(5)
print("[VIEWER] Server listening on {}:{}".format(server,port))

iter = 0
while(True):
    # read in the messages
    client_sock, address = serversocket.accept()
    print("[VIEWER] got connection from " + str(address))
    # total_bytes = 0
    # recstr = ""
    # gotstr = client_sock.recv(1024).decode()
    # recstr += gotstr
    # total_bytes += sys.getsizeof(gotstr)
    # while(gotstr):
    #   gotstr = client_sock.recv(1024)
    #   dcstr = gotstr.decode()
    #   recstr += dcstr
    #   total_bytes += sys.getsizeof(gotstr)
    recstr = mon_recv(client_sock)

    client_sock.close()

    f = StringIO(recstr)
    print("[VIEWER] length of recstr {}".format(len(recstr)))
    reader = csv.reader(f, delimiter=',')
    print("[VIEWER] num rows of recstr {}".format(reader.line_num))
    # plot
    devs = []
    ts = []
    freqs = []
    powers = []
    for i,row in enumerate(reader):
        if i == 0:
            continue # skip the first row as it is just a header
        try:
            devs.append(row[0])
            ts.append(row[1])
            freqs.append(float(row[2]))
            powers.append(float(row[3]))
        except IndexError as e:
            print("[VIEWER] Exception raised while parsing received data string")
            print("[VIEWER] row: {}. Line num {}".format(row,reader.line_num))
            print("[VIEWER] last frequency {}".format(freqs[-1]))
            
            
    print("[VIEWER] plotting {} points. Shapes: power-{}, freqs-{}".format(len(freqs), np.array(powers).shape, np.array(freqs).shape))
    print("[VIEWER] plotting {} points. Shapes: power-{}, freqs-{}".format(len(freqs), np.array(powers).shape, np.array(freqs).shape))
    print("[VIEWER] num rows of recstr {}".format(reader.line_num))
    if lines:
        lines.remove()
    lines, = plt.plot(np.array(freqs), np.array(powers), 'b')
    plt.draw()
    plt.pause(0.001)
    print("\033[93m[VIEWER] max power {} at frequency {}  \033[0m ".format(max(powers[1000:]), freqs[np.argmax(powers[1000:])]))
    print("\[VIEWER] mean power {:.2f} ".format(np.array(powers).mean()))
    print("\[VIEWER] powers[1000] {} ".format(powers[1000]))
    print("\[VIEWER] freqs[1000] {} ".format(freqs[1000]))
    nfreqs = np.array(freqs)
    args2400 = np.nonzero((nfreqs > 2399.9) & (nfreqs < 2400.1))
    npowers = np.array(powers)
    print("\[VIEWER] power @cf = {}, {} ".format(nfreqs[args2400], npowers[args2400]))
    
    iter += 1
